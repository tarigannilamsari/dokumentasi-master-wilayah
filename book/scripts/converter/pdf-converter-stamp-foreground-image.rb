class PDFConverterStampForegroundImage < (Asciidoctor::Converter.for 'pdf')
  register_for 'pdf'

  # these code are copied and modified from original code
  def stamp_foreground_image doc, has_front_cover
    pages = state.pages

    if (first_page = (has_front_cover ? (pages.slice 1, pages.size) : pages).find {|it| !it.imported_page? }) &&
        # modified! (first_page_num = (pages.index first_page) + 1) &&
        (first_page_num = (pages.index first_page) + 2) &&
        (fg_image = resolve_background_image doc, @theme, 'page-foreground-image') && fg_image[0]
      go_to_page first_page_num
      create_stamp 'foreground-image' do
        canvas { image fg_image[0], ({ position: :center, vposition: :center }.merge fg_image[1]) }
      end
      # not applied! stamp 'foreground-image'
      (first_page_num.next..page_count).each do |num|
        go_to_page num
        stamp 'foreground-image' unless page.imported_page?
      end
    end
  end
end
