module Helper
  class Git
    def self.log
      return `git log -1 --format=medium`
    end

    def self.branch_current
      return `git branch --show-current`.strip
    end

    def self.is_dirty
      return `git diff --stat` != ''
    end

    def self.is_current_branch_clean(selected_branch)
      return !self.is_dirty && self.branch_current === selected_branch
    end

    def self.date
      return `git log --date=short --pretty="format:#{self.branch_current}-%h, %cd" -1`
    end

    def self.remote
      result = `git remote get-url origin`
    end
  end
end
