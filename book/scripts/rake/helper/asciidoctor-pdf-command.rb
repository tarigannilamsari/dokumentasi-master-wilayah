module Helper
  class AsciidoctorPdfCommand
    def initialize(is_os_windows)
      @is_os_windows = is_os_windows
      @command = 'asciidoctor-pdf'
      @attributes = {}
      @libraries = {}
      @flags = {}
    end

    def add_attribute(name, value = nil)
      if value.is_a?(String) && value.length > 0
        @attributes[name] = "#{name}=#{quoted(value)}"
      else
        @attributes[name] = name
      end

      return self
    end

    def get_attributes
      return @attributes.values.map { |value| "-a #{value}" }
    end

    def add_library(library)
      @libraries[library] = library

      return self
    end

    def get_libraries
      return @libraries.values.map { |value| "-r #{quoted(value)}" }
    end

    def add_flag(name, value = nil)
      if value.is_a?(String) && value.length > 0
        @flags[name] = "#{name} #{quoted(value)}"
      else
        @flags[name] = name
      end

      return self
    end

    def get_flags
      return @flags.values.map { |value| "--#{value}" }
    end

    def create_arguments(input)
      return [@command, quoted(input)]
        .concat(get_libraries)
        .concat(get_attributes)
        .concat(get_flags)
    end

    private

    def quoted(str)
      return @is_os_windows ? "\"#{str}\"" : "'#{str}'"
    end
  end
end
