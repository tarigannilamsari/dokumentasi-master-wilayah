module Helper
  require "asciidoctor"

  def self.read_adoc_file(file_name)
    return Asciidoctor.load_file(file_name)
  end

  class AsciidoctorDocument
    def initialize(file_name)
      if !File.file?(file_name)
        raise "File '#{file_name}' not found!"
      end

      @document = Helper.read_adoc_file(file_name)

      title, subtitle = @document.doctitle.gsub('<br>', '').squeeze(' ').split(':')

      @title = title.strip
      @subtitle = subtitle.strip
    end

    def get_title
      return @title
    end

    def get_subtitle
      return @subtitle
    end

    def get_document
      return @document
    end
  end
end
