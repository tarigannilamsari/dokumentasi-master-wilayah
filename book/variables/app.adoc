:app-name: pass:q[*WRITE APPLICATION NAME HERE*]
:platform-name: pass:q[*WRITE PLATFORM NAME HERE*]

// Set API URL endpoint here, must set variable with their context
// declared (change CTX placeholder), and if available add this suffix:
// - dev (development)
// - stag (staging)
// - prod (production)
// At last variable, set one to be used as main URL. Example:
// api-be-url-dev, api-be-url-stag, api-be-url-prod, api-be-url
:api-CTX-url-dev: https://dev.api.tld
:api-CTX-url-stag: https://stag.api.tld
:api-CTX-url-prod: https://prod.api.tld
:api-CTX-url: {api-CTX-url-dev}
