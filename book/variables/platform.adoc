// google cloud platform
:p-gcp: pass:q[*Google Cloud Platform*]
:p-gcp-cloud-run: pass:q[*Cloud Run*]
:p-gcp-cloud-sql: pass:q[*Cloud SQL*]
:p-gcp-gke: pass:q[*Google Kubernetes Engine*]
:p-gcp-bigquery: pass:q[*Google BigQuery*]
:p-gcp-pubsub: pass:q[*Google Pub/Sub*]
:p-gcp-data-studio: pass:q[*Google Data Studio*]
:p-gcp-firebase: pass:q[*Firebase*]
:p-gcp-apigee: pass:q[*Apigee*]
:p-gcp-cloud-armor: pass:q[*Google Cloud Armor*]
// nodejs
:p-node: pass:q[*Node.js®*]
// .NET
:p-dotnet: pass:q[*.NET Core*]
