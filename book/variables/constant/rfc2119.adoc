// based on https://www.rfc-editor.org/rfc/rfc2119, to indicate mandatory level
:rfc-guide: pass:q[*RFC 2119*]
:rfc-must: pass:q[*WAJIB*]
:rfc-must-not: pass:q[*TIDAK BOLEH*]
:rfc-required: pass:q[*PERLU*]
:rfc-should: pass:q[*SEBAIKNYA*]
:rfc-should-not: pass:q[*SEBAIKNYA TIDAK*]
:rfc-recommended: pass:q[*DIREKOMENDASIKAN*]
:rfc-not-recommended: pass:q[*TIDAK DIREKOMENDASIKAN*]
:rfc-may: pass:q[*BOLEH*]
:rfc-optional: pass:q[*OPSIONAL*]
