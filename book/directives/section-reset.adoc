// section reset
// 1-6
ifeval::[{reset-sectlevel} == 1]
:!level1:
endif::[]

ifeval::["{reset-sectlevel}" == "1-2"]
:!level1:
:!level2:
endif::[]

ifeval::["{reset-sectlevel}" == "1-3"]
:!level1:
:!level2:
:!level3:
endif::[]

ifeval::["{reset-sectlevel}" == "1-4"]
:!level1:
:!level2:
:!level3:
:!level4:
endif::[]

ifeval::["{reset-sectlevel}" == "1-5"]
:!level1:
:!level2:
:!level3:
:!level4:
:!level5:
endif::[]

ifeval::["{reset-sectlevel}" == "1-6"]
:!level1:
:!level2:
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

ifeval::["{reset-sectlevel}" == "all"]
:!level1:
:!level2:
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

// 2-6
ifeval::[{reset-sectlevel} == 2]
:!level2:
endif::[]

ifeval::["{reset-sectlevel}" == "2-3"]
:!level2:
:!level3:
endif::[]

ifeval::["{reset-sectlevel}" == "2-4"]
:!level2:
:!level3:
:!level4:
endif::[]

ifeval::["{reset-sectlevel}" == "2-5"]
:!level2:
:!level3:
:!level4:
:!level5:
endif::[]

ifeval::["{reset-sectlevel}" == "2-6"]
:!level2:
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

ifeval::["{reset-sectlevel}" == "2+"]
:!level2:
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

// 3-6
ifeval::[{reset-sectlevel} == 3]
:!level3:
endif::[]

ifeval::["{reset-sectlevel}" == "3-4"]
:!level3:
:!level4:
endif::[]

ifeval::["{reset-sectlevel}" == "3-5"]
:!level3:
:!level4:
:!level5:
endif::[]

ifeval::["{reset-sectlevel}" == "3-6"]
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

ifeval::["{reset-sectlevel}" == "3+"]
:!level3:
:!level4:
:!level5:
:!level6:
endif::[]

// 4-6
ifeval::[{reset-sectlevel} == 4]
:!level4:
endif::[]

ifeval::["{reset-sectlevel}" == "4-5"]
:!level4:
:!level5:
endif::[]

ifeval::["{reset-sectlevel}" == "4-6"]
:!level4:
:!level5:
:!level6:
endif::[]

ifeval::["{reset-sectlevel}" == "4+"]
:!level4:
:!level5:
:!level6:
endif::[]

// 5-6
ifeval::[{reset-sectlevel} == 5]
:!level5:
endif::[]

ifeval::["{reset-sectlevel}" == "5-6"]
:!level5:
:!level6:
endif::[]

ifeval::["{reset-sectlevel}" == "5+"]
:!level5:
:!level6:
endif::[]

// 6
ifeval::[{reset-sectlevel} == 6]
:!level6:
endif::[]

// end section reset
