extends: default-for-print-with-font-fallbacks
base:
  text_align: justify
page:
  margin: [0.6in, 0.5in, 0.5in, 0.5in]
link:
  font_color: 428BCA
codespan:
  font_color: AB1D1D
button:
  background_color: F5F5F5
  border_color: CCCCCC
  border_offset: 2
  border_radius: 2
  border_width: 0.5
  font_family: $codespan_font_family
  content: "  %s  "
heading:
  h2:
    text_align: center
    line_height: 2
    font_style: bold
    font_color: 008E87
    text_transform: uppercase
  h3:
    font_color: 008E87
    font_style: bold
  h4:
    font_color: 008E87
    font_style: bold
  h5:
    font_style: bold
  h6:
    font_style: bold
title_page:
  text_align: center
  title:
    top: 45%
    font_size: $heading_h1_font_size * 1.25
    font_color: 008E87
    font_style: bold
    line_height: 0.9
  subtitle:
    font_size: $heading_h3_font_size * 1.25
    font_color: 008E87
    font_style: bold
    line_height: 1
  authors:
    margin_top: $base_font_size * 1.25
    font_size: $base_font_size_large
    font_color: 181818
  revision:
    margin_top: $base_font_size * 1.25
    font_color: 181818
block:
  margin_top: 0
  margin_bottom: $vertical_rhythm
caption:
  align: center
  font_style: italic
  text_align: center
admonition:
  background_color: FFFEF7
  border_color: BBBBBB
  border_width: $base_border_width
  border_radius: $base_border_radius
  column_rule_color: DDDDDD
  column_rule_width: $base_border_width
  label:
    text_align: center
    text_transform: uppercase
    font_style: bold
  icon:
    note:
      name: fas-paperclip
    tip:
      name: far-lightbulb
    warning:
      name: fas-exclamation-triangle
    important:
      name: fas-exclamation-circle
    caution:
      name: far-hand-paper
# code is used for literal, listing, and source blocks and literal table cells
code:
  font_color: $base_font_color
  font_family: $codespan_font_family
  font_size: 10
  padding: 3
  line_height: 1.25
  # line_gap is an experimental property to control how a background color is applied to an inline block element
  line_gap: 3.8
  background_color: FFF8ED
  border_color: CCCCCC
  border_radius: 0
  border_width: 0.75
conum:
  font_family: $codespan_font_family
  font_color: $codespan_font_color
  font_size: $base_font_size
  line_height: 4 / 3
  glyphs: 1-100
image:
  align: center
  border-color: CCCCCC
  border-radius: 2
  border-width: 0.5
list:
  item_spacing: 2
callout_list:
  margin_top_after_code: -$block_margin_bottom / 2
table:
  background_color: $page_background_color
  border_color: 205743
  border_width: $base_border_width
  grid_width: $base_border_width
  cell_padding: 3
  head:
    background_color: 008E87
    font_style: bold
    font_color: FFFFFF
    border_bottom_color: 205743
    border_bottom_width: $base_border_width * 2.5
  body:
    stripe_background_color: F9F9F9
  foot:
    background_color: F0F0F0
toc:
  indent: $horizontal_rhythm
  line_height: 1.4
  title:
    align: center
    text_transform: uppercase
  dot_leader:
    font_color: A9A9A9
    levels: all
  h2:
    font_style: bold
    font_color: 008E87
    text_transform: uppercase
  h3:
    font_color: 008E87
  h4:
    font_color: 008E87
header:
  border_color: 008E87
  border_width: 0.5
  height: $base_line_height_length * 3
  line_height: 0.75
  padding: [$base_line_height_length / 2, 0.5, 0, 0.5]
  vertical_align: top
  image_vertical_align: middle
  columns: <12% =70% >18%
  recto:
    left:
      content: image::logo-kemkes.png[fit=contain]
    center:
      font_size: 7.5
      content: |
        pass:q,a[*{book-title}*] +
        pass:q,a[*{book-platform}*] +
        pass:q,a[{book-subtitle}]
    right:
      font_size: 8
      content: |
        pass:q,a[<font color="#EE0000" size="8.5">*{revremark}*</font>] +
        versi {revnumber} +
        {revdate}
  verso:
    left:
      content: $header_recto_left_content
    center:
      font_size: 8
      content: $header_recto_center_content
    right:
      font_size: 8.5
      content: $header_recto_right_content
footer:
  border_color: 008E87
  border_width: 0.75
  height: $base_line_height_length * 2.5
  line_height: 1
  padding: 0.5
  vertical_align: middle
  columns: <85% >15%
  recto:
    left:
      font_size: 6
      content: pass:q,a[{disclaimer}]
    right:
      font_color: 008E87
      font_style: bold
      content: |
        Halaman +
        {page-number} dari {page-count}
  verso:
    left:
      font_size: 6
      content: $footer_recto_left_content
    right:
      font_color: 40AE87
      font_style: bold
      content: $footer_recto_right_content
role:
  ref:
    font_color: 428BCA
  confidentiality-level:
    font_color: FF0000
    font_style: bold
    font_size: 30
