:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

Beberapa simbol, istilah, dan konvensi digunakan pada pedoman ini agar penyampaian konsep, struktur
data, baik ke tim _developer_ maupun non _developer_ menjadi lebih jelas. Pada bagian ini akan
dijelaskan lebih lanjut terkait hal tersebut.

include::{dir-snippet}/admonition/tip-text-blue.adoc[]


=== {counter:level1}. Kata Kunci Prioritas

include::rfc2119.adoc[]


=== {counter:level1}. Simbol dan Istilah

include::symbols.adoc[]


=== {counter:level1}. ReST API

include::rest.adoc[]


=== {counter:level1}. ISO 8601

include::iso8601.adoc[]


=== {counter:level1}. Pesan Prioritas

include::admonitions.adoc[]


<<<
