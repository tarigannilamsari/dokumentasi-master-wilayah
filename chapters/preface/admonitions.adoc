:reset-sectlevel: 2+
include::{dir-directive}/section-reset.adoc[]

Beberapa pesan prioritas berisi pernyataan, peringatan, perhatian, atau teguran tertentu sebagai salah satu cara untuk menekankan beberapa penjelasan yang dianggap penting, digunakan pada dokumentasi ini. Tipe-tipe dari kotak prioritas tersebut antara lain:


==== {level1}.{counter:level2}. Catatan

Memberikan catatan singkat terkait informasi tertentu. Contoh:

NOTE: Postman adalah salah satu aplikasi untuk melakukan _testing_ terkait ReST API yang _user-friendly_ dibandingkan cURL.


==== {level1}.{counter:level2}. Tips

Memberikan saran kepada pembaca yang terkait _best practices_ atau tips tertentu. Contoh:

TIP: Untuk versi cURL terbaru, gunakan parameter `--oauth2-bearer` untuk melakukan _request_ yang membutuhkan _token bearer_.


==== {level1}.{counter:level2}. Penting

Memberikan saran kepada pembaca bahwa anjuran yang disebutkan penting untuk dibaca. Contoh:

IMPORTANT: Selalu pakai _token bearer_ yang didapatkan setiap melakukan _request_ API yang bukan untuk autentikasi.


==== {level1}.{counter:level2}. Perhatian

Memberikan saran kepada pembaca agar anjuran yang telah dijelaskan dilakukan dengan hati-hati. Contoh:

CAUTION: Mohon diperhatikan saat melakukan _request_ data, dikarenakan ada perbedaan target URL untuk versi _production_ dan _development_.


==== {level1}.{counter:level2}. Peringatan

Memberikan tekanan kepada pembaca agar anjuran yang telah dijelaskan dilakukan dengan serius dan hati-hati, dikarenakan kemungkinan akan ada efek samping. Contoh:

WARNING: Cek apakah _payload_ yang akan dikirimkan sudah sesuai standar yang disepakati sebelumnya, karena bila tidak sesuai _payload_ tersebut tidak akan dapat diproses.
