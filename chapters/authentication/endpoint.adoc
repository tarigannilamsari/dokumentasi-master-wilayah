[#auth-endpoint]
=== {counter:level1}. Endpoint

Pada bagian ini akan dijelaskan spesifikasi untuk {auth-endpoint-name}, yang mempunyai _endpoint_ berdasarkan jenis lingkungan pengembangannya (_development environment_) yaitu:

* _development_ {auth-api-url-dev}
* _staging_ {auth-api-url-stg}
* _production_ {auth-api-url-prod}

include::{dir-snippet}/admonition/note-endpoint-prod-only.adoc[]
