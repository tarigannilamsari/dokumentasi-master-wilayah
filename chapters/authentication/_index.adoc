:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

:api-context: authentication
:api-endpoint: {auth-api-url}

include::intro.adoc[]
include::endpoint.adoc[]
include::postman-collection.adoc[]
include::registration.adoc[]
include::apis.adoc[]
