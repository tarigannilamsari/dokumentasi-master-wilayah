[preface]
== Pengantar Teknis
include::preface/_index.adoc[]

== {counter:chapter:A}. Pendahuluan
include::preliminary/_index.adoc[]

== {counter:chapter}. Autentikasi
include::authentication/_index.adoc[]

== {counter:chapter}. Master Wilayah Versi 1
include::products-v1/_index.adoc[]

// == {counter:chapter}. ReST API Front-end
// include::frontend-api/_index.adoc[]

[glossary]
== Daftar Istilah
include::glossary/_index.adoc[]

[bibliography]
== Referensi
include::bibliography/_index.adoc[]
