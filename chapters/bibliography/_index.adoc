:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

// Reference format:
// * [[[reference-label]]]
// Author. (optional)
// "Book/Article Title".
// Publisher. (optional)
// Site URL. (optional)

* [[[restfulapi.net]]]
"ReST API Tutorial".
https://restfulapi.net.

* [[[wikipedia-rest]]]
"Representational State Transfer".
https://en.wikipedia.org/wiki/Representational_state_transfer.

* [[[wikipedia-http-method]]]
"Hypertext Transfer Protocol, Request_methods".
https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods

* [[[wikipedia-http-status-code]]]
"List of HTTP status codes".
https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

* [[[wikipedia-iso8601]]]
"ISO 8601".
https://en.wikipedia.org/wiki/ISO_8601

* [[[wikipedia-api]]]
"Application Programming Interface".
https://en.wikipedia.org/wiki/API

* [[[wikipedia-whitespace]]]
"Whitespace character".
https://en.wikipedia.org/wiki/Whitespace_character

* [[[wikipedia-fhir]]]
"Fast_Healthcare_Interoperability_Resources",
https://en.wikipedia.org/wiki/

* [[[hl7-fhir]]]
"Welcome to FHIR. FHIR is a standard for health care data exchange, published by HL7®".
https://www.hl7.org/fhir/index.html

* [[[satusehat-fhir]]]
"SATUSEHAT FHIR R4 Implementation Guide".
https://simplifier.net/guide/SATUSEHAT-FHIR-R4-Implementation-Guide/Home?version=current

* [[[admonitions]]]
"Admonitions".
https://docs.asciidoctor.org/asciidoc/latest/blocks/admonitions

* [[[atc-code]]]
"Daftar Kode ATC"
https://id.wikipedia.org/wiki/Kategori:Kode_ATC

* [[[atc-level]]]
"Daftar Level ATC"
https://www.who.int/tools/atc-ddd-toolkit/atc-classification
