[source,erlang]
----
DATA STRUCTURE:
{ <.>
  *status_code: integer <.>
  *message: string <.>
  *page: integer <.>
  *total_page: integer <.>
  *data: [{ <.>
    *kode_satusehat: string <.>
    *kode_sarana: string <.>
    *nama: string <.>
    *telp: string <.>
    *email: string <.>
    *website: string <.>
    *longitude: string <.>
    *latitude: string <.>
    *operasional: boolean <.>
    *wilayah_perairan_darat: string <.>
    *wilayah_karakteristik: string <.>
    *sarana_administrasi: { <.>
      *kode: string <.>
      *nama: string <.>
      *kode_sarana: string <.>
      *status_aktif: boolean <.>
      *status_sarana: string <.>
    }
    *alamat: string <.>
    *provinsi: { <.>
      *kode: number <.>
      *nama: string <.>
      *kode_bps: string <.>
      *kode_lama: string <.>
    }
    *kabkota: { <.>
      *kode: number <.>
      *nama: string <.>
      *kode_bps: string <.>
      *kode_lama: string <.>
    }
    *jenis_sarana: { <.>
      *kode: string <.>
      *nama: string <.>
      *nama_alt: string <.>
    }
    *subjenis: { <.>
      *kode: string <.>
      *nama: string <.>
      *nama_alt: string <.>
    }
    *kelas_sarana: { <.>
      *kode: string <.>
      *nama: string <.>
    }
    *status_sarana: string <.>
    *status_aktif: boolean <.>
  }]
}
----

<.> Respon yang diterima berupa `object`.
<.> Properti `status_code` bertipe `integer`, berisi informasi kode hasil respon yang diterima
<.> Properti `message` bertipe `string`, berisi informasi hasil dari respon yang diterima
<.> Properti `page` bertipe `integer`, berisi berapa baris halaman yang ingin ditampilkan
<.> Properti `total_page` bertipe `integer`, total dari hasil pencarian
<.> Properti `data` bertipe `array of objects`, bila kosong akan mengembalikan array kosong. Setiap object item berisi data FASYANKES (Rumah Sakit, Klinik, Puskesmas, Praktek Mandiri).
<.> Properti `kode_satusehat` bertipe `string`, berisi informasi kode satu sehat (10 digit)
<.> Properti `kode_sarana` bertipe `string`, berisi informasi kode FASYANKES
<.> Properti `nama` bertipe `string`, berisi informasi nama FASYANKES
<.> Properti `telp` bertipe `string`, berisi informasi nomer telepon FASYANKES
<.> Properti `email` bertipe `string`, berisi informasi alamt _email_ FASYANKES
<.> Properti `website` bertipe `string`, berisi informasi _website_ FASYANKES
<.> Properti `longtitude` bertipe `string`, garis bujur contoh : 106.821810
<.> Properti `latitude` bertipe `string`, garis lintang contoh : -6.193125
<.> Properti `operasional` bertipe `boolean`, mengindikasikan apakah ada sarana FASYANKES beroperasi (_true_) atau tidak (_false_).
<.> Properti `wilayah_perairan_darat` bertipe `string`, wilayah perairan darat
<.> Properti `wilayah_karakteristik` bertipe `string`, wilayah karakteristik
<.> Properti `sarana_administrasi` bertipe `object`, data sarana administrasi
<.> Properti `kode` bertipe `string`, kode satusehat (10 digit) dari FASYANKES yang dinyatakan sebagai induk untuk administratif
<.> Properti `nama` bertipe `string`, nama FASYANKES yang dinyatakan sebagai induk untuk administratif
<.> Properti `kode_sarana` bertipe `string`, kode FASYANKES dari sumber data utama yang dinyatakan sebagai induk untuk administratif
<.> Properti `status_aktif` bertipe `string`, status keaktifan dari FASYANKES (_true_) atau tidak (_false_)
<.> Properti `status_sarana` bertipe `string`, status administrasi dari FASYANKES yang dinyatakan sebagai induk untuk administratif yang diisi *_draft_*, *_review_*, *_verified_*, atau *_valid_*
<.> Properti `alamat` bertipe `string`, berisi informasi alamat FASYANKES
<.> Properti `provinsi` bertipe `object`, berisi data letak provinsi FASYANKES
<.> Properti `kode` bertipe `string`, berisi informasi kode dagri provinsi FASYANKES
<.> Properti `nama` bertipe `string`, berisi informasi nama provinsi FASYANKES
<.> Properti `kode_bps` bertipe `string`, kode BPS provinsi FASYANKES
<.> Properti `kode_lama` bertipe `string`, kode dagri provinsi yang lama (sebelum pemekaran) FASYANKES
<.> Properti `kabkota` bertipe `object`, berisi data letak kabupaten/kota sarana
<.> Properti `kode` bertipe `string`, berisi informasi kode dagri kabupaten/kota
<.> Properti `nama` bertipe `string` berisi informasi nama kabupaten/kota
<.> Properti `kode_bps` bertipe `string`, kode BPS kabupaten kota FASYANKES
<.> Properti `kode_lama` bertipe `string`, kode dagri kabupaten kota yang lama (sebelum pemekaran) FASYANKES
<.> Properti `jenis_sarana` bertipe `object`, berisi data jenis sarana
<.> Properti `kode` bertipe `string`, berisi informasi id jenis sarana
<.> Properti `nama` bertipe `string` berisi informasi deskripsi atau nama dari jenis sarana
<.> Properti `nama_alt` bertipe `string` berisi informasi nama alternatif dari jenis sarana
<.> Properti `subjenis` bertipe `object`, berisi data sub-jenis sarana
<.> Properti `kode` bertipe `string`, berisi informasi id sub-jenis sarana
<.> Properti `nama` bertipe `string`, berisi informasi deskripsi atau nama dari jenis sarana
<.> Properti `nama_alt` bertipe `string`, berisi informasi nama alternatif dari jenis sarana
<.> Properti `kelas_sarana` bertipe `object`, berisi data dari kelas sarana
<.> Properti `kode` bertipe `string`, berisi informasi id kelas sarana
<.> Properti `nama` bertipe `string`, berisi informasi deskripsi atau nama dari kelas sarana
<.> Properti `status_sarana` bertipe `string`, berisi informasi status administrasi dari FASYANKES yang diisi *_draft_*, *_review_*, *_verified_*, atau *_valid_*
<.> Properti `status_aktif` bertipe `boolean`, berisi informasi dari status keaktifan sarana FASYANKES (_true_) atau tidak (_false_).
