|`*{http-header-authz}`
|`string`
|_Header_ ini {rfc-must} diisi dengan nilai sesuai format: `Bearer {access-token-var}`. Nilai dari variabel `{access-token-var}` didapatkan dari properti `{access-token-path}` pada `object` dari hasil _response_ JSON setelah proses autentikasi.
