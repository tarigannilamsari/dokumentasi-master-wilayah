|`*{http-header-content}`
|`string`
|_Mime type_ dari _payload_ data yang akan dikirimkan di dalam _body_ dalam bentuk _URL Encoded_, {rfc-must} diisi dengan `{mime-urlencoded}`
